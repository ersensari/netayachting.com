﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using netayachting.com.Data;
using netayachting.com.Models;

namespace netayachting.com
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddDbContext<dbContext>(options =>
       options
       .UseLazyLoadingProxies(false)
       .UseSqlServer(Configuration.GetConnectionString("dbContext"))
       .ConfigureWarnings(warnings => warnings.Throw(CoreEventId.IncludeIgnoredWarning)));

      services.AddIdentity<ApplicationUser, IdentityRole>()
        .AddEntityFrameworkStores<dbContext>()
        .AddDefaultTokenProviders();

      services.ConfigureApplicationCookie(options =>
      {
        options.LoginPath = "/Account/Login";
        options.LogoutPath = "/Account/Logout";
        options.AccessDeniedPath = "/Account/AccessDenied";
        options.SlidingExpiration = true;
        options.Cookie = new CookieBuilder
        {
          HttpOnly = true,
          Name = "netayachting.com.Security.Cookie",
          Path = "/",
          SameSite = SameSiteMode.Lax,
          SecurePolicy = CookieSecurePolicy.SameAsRequest
        };
      });

      services.AddAuthorization(options =>
      {
        options.AddPolicy("AllRoles", policy =>
         policy.RequireRole("Admin", "Editor", "Insan_Kaynaklari", "Call_Center"));

      });

      services.AddMvc().AddJsonOptions(
        options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
      );
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      app.UseDeveloperExceptionPage();
      if (env.IsDevelopment())
      {
        app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
        {
          HotModuleReplacement = true
        });
      }
      else
      {
        app.UseExceptionHandler("/Home/Error");
      }

      app.UseStaticFiles(new StaticFileOptions()
      {
        ServeUnknownFileTypes = true
      });

      app.UseDatabaseErrorPage();

      app.UseAuthentication();

      app.UseMvc(routes =>
      {
        routes.MapRoute(
          name: "default",
          template: "{controller=Home}/{action=Index}/{id?}");

        routes.MapSpaFallbackRoute(
          name: "spa-fallback",
          defaults: new { controller = "Home", action = "Index" });

        routes.MapSpaFallbackRoute(
          name: "spa-fallback-admin",
          defaults: new { controller = "Admin", action = "Index" });
      });
    }
  }
}
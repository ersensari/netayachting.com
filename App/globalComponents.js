﻿import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-theme.css";

// import "froala-editor/css/froala_editor.pkgd.min.css";
import "font-awesome/css/font-awesome.css";

// import "froala-editor/css/froala_style.min.css";
import "waypoints/lib/jquery.waypoints";
import "vue-multiselect/dist/vue-multiselect.min.css";
import "@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css";

import "./css/jquery.mmenu.all.css";
import "./css/jquery.mhead.css";
import "./css/hamburgers.min.css";
import "./css/themify-icons.css";
import "./css/icofont.css";
import "./css/datepicker.css";
import "./css/animate.css";
import "./css/bootstrap-touch-slider.css";
import "./css/style.css";

import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel";

import "./js/jquery.touchSwipe.min.js";
import "./js/bootstrap-touch-slider.js";

import "./js/jquery.filterizr.min.js";
import "./js/jquery.easing.min.js";
import "./js/jquery-ui.min.js";
import "./js/jquery.counterup.min.js";
import "./js/jquery.sticky.js";
import "./js/datepicker.js";
import "./js/i18n/datepicker.tr-TR";

import "./js/jquery.mmenu.all.js";
import "./js/jquery.mhead.js";


﻿export default {
  initFleetCarousels() {
    $('#fleet-carousel').owlCarousel('destroy');
    $('#fleet-carousel').owlCarousel({
      items: 2,
      margin: 10,
      loop: true,
      autoplay: true,
      smartSpeed: 1000,

      nav: true,
      navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],

      dots: false,
      autoplayHoverPause: true,
      autoHeight: true
    });

    $('#fleet-other-carousel').owlCarousel('destroy');
    $('#fleet-other-carousel').owlCarousel({
      items: 6,
      margin: 10,
      loop: true,
      autoplay: true,
      smartSpeed: 1000,

      nav: true,
      navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],

      dots: false,
      autoplayHoverPause: true,
      autoHeight: true
    });
  },
  initDestinationCarousels() {
    $('#dest-carousel').owlCarousel({
      items: 3,
      margin: 10,
      loop: true,
      autoplay: true,
      smartSpeed: 1000,

      nav: true,
      navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],

      dots: false,
      autoplayHoverPause: true,
      autoHeight: true
    });
  }
}
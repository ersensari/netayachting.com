//  "use strict";


export default {
    initPage() {

        $(window).on('scroll', function () {

            if ($(this).scrollTop() > 600) {

                $('.return-to-top').fadeIn();

            } else {

                $('.return-to-top').fadeOut();

            }

        });

        $('.return-to-top').on('click', function () {

            $('html, body').animate({

                scrollTop: 0

            }, 1500, () => document.location.hash = "#top"
        )
            ;

            return false;

        });


        function toggleMenu() {
            if ($(window).width() <= 1200) {
                $(".mh-header").show();
                $(".logo-image").hide();
                $(".top-container").unstick();
            } else {
                $(".mh-header").hide();
                $(".logo-image").show();
                $(".top-container").sticky({topSpacing: 0});
            }
        }

        $(window).on("resize", function () {
            toggleMenu();
        });

        $(document).ready(function () {
            try {
                $("#sidebarNew").mmenu(
                    {
                        extensions: ["pagedim-black"]
                    },
                    {
                        offCanvas: {
                            pageNodetype: "section"
                        }
                    }
                );

                $(".mh-head").mhead();

                $('[data-toggle="datepicker"]').datepicker({
                    language: "tr-TR",
                    autoHide: true,
                    startView: 2
                });
            } catch (e) {
                console.log(e);
            }

            toggleMenu();

              setTimeout(function () {
                $('.carousel').bsTouchSlider();
              },2000);
        });

        $(window).resize(function () {
            var windowWidth = $(window).width();

            if (windowWidth > 768) {
                stick();
            } else {
                unstick();
            }
        });

        function stick() {
            $(".top-container").sticky({
                topSpacing: 0,
                responsiveWidth: true
            });
        }

        function unstick() {
            $('.navbar').unstick();
        }

        $(document).ready(function () {

            //loader
            $("body").addClass("loaded");
            $(".loader").fadeOut(1000);

            setTimeout(function () {
                $('#bootstrap-touch-slider').bsTouchSlider();
            },1000);

        });
    },
    initCarousel() {
        $(window).on('load', function () {

            setTimeout(function () {


                $(".our-specialist-carousel").owlCarousel({
                    loop: true,
                    margin: 10,
                    responsiveClass: true,
                    items: 4,
                    autoPlay: true,
                    lazyLoad: true,
                    responsive: {
                        0: {
                            items: 1,
                            nav: true
                        },
                        900: {
                            items: 3,
                            nav: false
                        },
                        600: {
                            items: 2,
                            nav: false
                        },
                        1000: {
                            items: 4,
                            nav: true,
                            loop: false
                        }
                    }
                });


                $(".our-company-carousel").owlCarousel({
                    responsiveClass: true,
                    items: 4,
                    loop: true,
                    margin: 10,
                    autoplay: true,
                    autoplayTimeout: 2000,
                    autoplayHoverPause: false,
                    responsive: {
                        0: {
                            items: 1,
                            nav: true,
                            loop: true
                        },
                        900: {
                            items: 3,
                            nav: false,
                            loop: true
                        },
                        600: {
                            items: 2,
                            nav: false,
                            loop: true
                        },
                        1000: {
                            items: 4,
                            nav: true,
                            loop: true
                        }
                    }
                });

                //var owl = $(".our-specialist-carousel");
                //owl.owlCarousel();

                // Go to the next item
                $(".customNextBtn").click(function () {
                    owl.trigger("next.owl.carousel");
                });

                // Go to the previous item
                $(".customPrevBtn").click(function () {
                    owl.trigger("prev.owl.carousel", [600]);
                });


            }, 4000);


        });
    }
}
﻿import Vue from "vue";
import VueRouter from "vue-router";
import store from "../AppAdmin/store";
import VueToastr from "@deveodk/vue-toastr";
import "bootstrap";
import GlobalComponents from "../AppAdmin/globalComponents";

import "./globalComponents";

import Content from "./Components/Pages/Content.vue";

import moment from "moment";
import numeral from 'numeral';
import App from "./App.vue";

Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(GlobalComponents);

Vue.use(VueToastr, {
  defaultPosition: "toast-bottom-right",
  defaultType: "info",
  defaultTimeout: 3000
});

Vue.filter("formatDateTime", function (value) {
  if (value) {
    return moment(String(value)).format("DD.MM.YYYY HH:mm");
  }
});

Vue.filter("formatDate", function (value) {
  if (value) {
    return moment(String(value)).format("DD.MM.YYYY");
  }
});

Vue.filter("formatDateMoment", function (value) {
  if (value) {
    return value.format("DD.MM.YYYY");
  }
});

Vue.filter("formatDateForDotnet", function (value) {
  if (value) {
    return value.format("YYYY-MM-DD");
  }
});


Vue.filter("formatNumber", function (value) {
  return numeral(value).format("0,0"); 
});


const routes = [
  {
    path: "/",
    component: App
  },
  {
    path: "/:url/:param1?/:param2?/:param3?",
    component: Content
  }
];

const router = new VueRouter({
  routes,
  mode: "history",

  scrollBehavior(to) {
    if (to.hash) {
      return $("html,body")
        .stop()
        .animate(
          { scrollTop: $(to.hash).offset().top - +170 },
          1000,
          "easeInOutExpo"
        );
    } else {
      return $("html,body")
        .stop()
        .animate({ scrollTop: 0 }, 500, "easeInOutExpo");
    }
  }
});

router.beforeEach((to, from, next) => {
  if (to.meta.anchor) {
    next({
      path: "/" + to.meta.anchor
    });
  } else {
    next();
  }
});

//DateTimePicker TR Lang
Object.defineProperty(Vue.prototype, "$lang", {
  get() {
    return {
      days: ["Paz", "Pzt", "Sal", "Çar", "Per", "Cum", "Cmt"],
      months: [
        "Oca",
        "Şub",
        "Mar",
        "Nis",
        "May",
        "Haz",
        "Tem",
        "Ağu",
        "Eyl",
        "Eki",
        "Kas",
        "Ara"
      ],
      pickers: ["7 gün ileri", "30 gün ileri", "7 gün geri", "30 gün geri"],
      placeholder: {
        date: "Tarih Seçiniz"
      }
    };
  }
});
//Fleet Categories
Object.defineProperty(Vue.prototype, "$YachtCategories", {
  get() {
    return [{ name: "TURKEY" }, { name: "GREECE" }, { name: "CROATIA" }];
  }
});
new Vue({
  el: "#app",
  router,
  store,
  template: `<router-view></router-view>`,
  watch: {
    $route(to) {
      this.$store.dispatch("links/getSingleByUrl", to.params.url);
    }
  }
});

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace netayachting.com.Data
{
  public enum GenderEnum
  {
    [DisplayName("Erkek")]
    Male = 1,
    [DisplayName("Kadın")]
    Female = 2
  }
}

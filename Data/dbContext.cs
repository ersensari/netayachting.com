﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Models;

namespace netayachting.com.Data {
  public class dbContext : IdentityDbContext<ApplicationUser> {
    public dbContext (DbContextOptions<dbContext> options) : base (options) { }

    public DbSet<Person> People { get; set; }
    public DbSet<Page> Pages { get; set; }
    public DbSet<Link> Links { get; set; }
    public DbSet<Gallery> Galleries { get; set; }
    public DbSet<Setting> Settings { get; set; }
    public DbSet<Blog> Blogs { get; set; }
    public DbSet<BlogGallery> BlogGalleries { get; set; }
    public DbSet<Resume> Resumes { get; set; }
    public DbSet<ImageSlider> ImageSliders { get; set; }
    public DbSet<City> Cities { get; set; }
    public DbSet<Town> Towns { get; set; }
    public DbSet<Yacht> Yachts { get; set; }
    public DbSet<YachtPrice> YachtPrices { get; set; }
    public DbSet<YachtGallery> YachtGalleries { get; set; }
    public DbSet<ContactForm> ContactForms { get; set; }
    public DbSet<Feedback> Feedbacks { get; set; }
    public DbSet<Currency> Currencies { get; set; }
    public DbSet<CurrencyRate> CurrencyRates { get; set; }
    public DbSet<Appointment> Appointments { get; set; }
    public DbSet<YachtPriceWeek> YachtPriceWeeks { get; set; }
  }
}
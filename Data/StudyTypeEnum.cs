﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace netayachting.com.Data
{
  public enum StudyTypeEnum
  {
    [DisplayName(displayName: "Okur Yazar")]
    OkurYazar = 1,
    [DisplayName(displayName: "İlkokul")]
    Ilkokul = 2,
    [DisplayName(displayName: "Ortaokul")]
    Ortaokul = 3,
    [DisplayName(displayName: "Lise")]
    Lise = 4,
    [DisplayName(displayName: "Ön Lisans")]
    Onlisans = 5,
    [DisplayName(displayName: "Lisans")]
    Lisans = 6,
    [DisplayName(displayName: "Yüksek Lisans")]
    YuksekLisans = 7,
    [DisplayName(displayName: "Doktora")]
    Doktora = 8
  }
}

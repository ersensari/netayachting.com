﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace netayachting.com.Models.AccountViewModels {
    public class LoginViewModel {
        [Required]
        [EmailAddress]
        [Display (Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType (DataType.Password)]
        [Display (Name = "Şifre")]
        public string Password { get; set; }

        [Display (Name = "Hatırla?")]
        public bool RememberMe { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netayachting.com.Models
{
  public class YachtSearchCriteria
  {
    public string Destination { get; set; }
    public int Month { get; set; }
    public string Date { get; set; }
    public int NumOfGuests { get; set; }
    public decimal Price { get; set; }
    public string Order { get; set; }
  }
}

﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;

namespace netayachting.com.Models
{
  public class YachtGallery
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string ImageThumbUrl { get; set; }
    public string ImageLargeUrl { get; set; }
    [ForeignKey("Yacht")]
    public int YachtId { get; set; }
    public bool Cover { get; set; }
    public virtual Yacht Yacht { get; set; }

    [NotMapped]
    public IFormFile File { get; set; }
  }
}

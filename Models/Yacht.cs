﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace netayachting.com.Models
{
  public class Yacht
  {
    public int Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    public string Destination { get; set; }
    public string PosibleDepartures { get; set; }
    public int NumberOfCabins { get; set; }
    public int MaxNumberOfGuests { get; set; }
    public string Length { get; set; }
    public string Beam { get; set; }
    public string Draft { get; set; }
    public string CruiseSpeed { get; set; }
    public string WaterTank { get; set; }
    public string CrewDescription { get; set; }
    public string Sailing { get; set; }
    public string CheckInTime { get; set; }
    public string CheckOutTime { get; set; }
    public string YachtDescription { get; set; }
    public string PriceDescription { get; set; }
    public string PriceDetailedDescription { get; set; }
    public string AccommodationDetails { get; set; }

    public bool Recommended { get; set; }
    public string Tags { get; set; }

    public int DisplayOrder { get; set; }

    public IEnumerable<YachtPrice> YachtPrices { get; set; }
    public IEnumerable<YachtGallery> Galleries { get; set; }
  }
}

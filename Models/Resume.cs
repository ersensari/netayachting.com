﻿using Microsoft.AspNetCore.Http;
using netayachting.com.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netayachting.com.Models
{
  public class Resume
  {
    [Key]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public GenderEnum Gender { get; set; }
    public DateTime Birthdate { get; set; }
    [ForeignKey("City")]
    public int CityId { get; set; }
    [ForeignKey("Town")]
    public int TownId { get; set; }
    public int Experience { get; set; }
    public StudyTypeEnum StudyType { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public string ResumeFileUrl { get; set; }
    public string CoverLetter { get; set; }

    public DateTime CreatedDate { get; set; }
    public DateTime? ModifiedDate { get; set; }

    public virtual City City { get; set; }
    public virtual Town Town { get; set; }

    [NotMapped]
    public IFormFile ResumeFile { get; set; }
  }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netayachting.com.Models
{
  public class Appointment
  {
    public int Id { get; set; }

    [Required(AllowEmptyStrings = false)]
    [StringLength(50)]
    public string Name { get; set; }

    [Required(AllowEmptyStrings = false)]
    [StringLength(50)]
    public string Surname { get; set; }

    [Required(AllowEmptyStrings = false)]
    [DataType(DataType.EmailAddress)]
    [EmailAddress]
    [StringLength(250)]
    public string Email { get; set; }

    [Required(AllowEmptyStrings = false)]
    [StringLength(50)]
    public string Phone { get; set; }

    [Required(AllowEmptyStrings = false)]
    [DataType(DataType.Text)]
    [StringLength(2000)]
    public string Notes { get; set; }

    public DateTime EmbarkationDate    { get; set; }
    public DateTime DisembarkationDate { get; set; }

    public int? NumberOfGuests { get; set; }
    public string  City { get; set; }
    public string State { get; set; }


    public DateTime CreatedDate { get; set; }
    public string IpAddress { get; set; }
    public bool Answered{ get; set; }

    public string YachtName { get; set; }
    public string Explanation { get; set; }
  }
}

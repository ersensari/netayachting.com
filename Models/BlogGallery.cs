﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;

namespace netayachting.com.Models
{
  public class BlogGallery
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string ImageThumbUrl { get; set; }
    public string ImageLargeUrl { get; set; }
    [ForeignKey("Blog")]
    public int BlogId { get; set; }

    public virtual Blog Blog { get; set; }

    [NotMapped]
    public IFormFile File { get; set; }
  }
}

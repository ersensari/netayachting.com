﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netayachting.com.Models
{
  public class Blog
  {
    public int Id { get; set; }
    public string Title { get; set; }
    public string HtmlContent { get; set; }
    public DateTime CreateDate { get; set; }
    public bool? HomePageSlider { get; set; }
    public int DisplayType { get; set; }
    [ForeignKey("Person")]
    public int? PersonId { get; set; }

    public virtual IEnumerable<BlogGallery> Galleries { get; set; }

    public virtual Person Person { get; set; }
  }
}

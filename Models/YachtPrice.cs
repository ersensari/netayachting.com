﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace netayachting.com.Models {
	public class YachtPrice {
		public int Id { get; set; }

		[ForeignKey ("Yacht")]
		public int YachtId { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public string Name { get; set; }
		public int Year { get; set; }
		public decimal Price { get; set; }
		public Nullable<bool> Active { get; set; }

		public virtual Yacht Yacht { get; set; }

		public IEnumerable<YachtPriceWeek> YachtPriceWeeks { get; set; }
	}

	public class YachtPriceWeek {
		public int Id { get; set; }
		public int WeekNo { get; set; }
		public int State { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }

		[ForeignKey ("YachtPrice")]
		public int YachtPriceId { get; set; }

		public virtual YachtPrice YachtPrice { get; set; }
	}
}
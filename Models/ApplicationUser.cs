using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace netayachting.com.Models {
  public class ApplicationUser : IdentityUser { }
}
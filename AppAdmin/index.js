﻿import Vue from "vue";
import VueRouter from "vue-router";
import store from './store'

require('../App/css/icofont.css');
// require('froala-editor/js/froala_editor.pkgd.min')

// Require Froala Editor css files.
// require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')
require('bootstrap/dist/css/bootstrap.css');
// require('froala-editor/css/froala_style.min.css')
require('vue-multiselect/dist/vue-multiselect.min.css');
require('@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css');
import * as uiv from 'uiv'
import locale from 'uiv/src/locale/lang/tr-TR'
Vue.use(uiv, { prefix: 'uiv', locale })

// import VueFroala from 'vue-froala-wysiwyg';
// Vue.use(VueFroala);

// Plugins
import GlobalComponents from "./globalComponents";
import GlobalDirectives from "./globalDirectives";

//import vueTippy from 'vue-tippy'

import SideBar from "./components/UIComponents/SidebarPlugin";
import VueToastr from '@deveodk/vue-toastr';

import App from "./App";

// router setup
import routes from "./routes/routes";

// library imports
import "./assets/sass/paper-dashboard.scss";
import "es6-promise/auto";
import moment from 'moment';
require('moment/locale/tr')



Vue.filter('formatDateTime', function (value) {
  if (value) {
    return moment(String(value)).format('DD.MM.YYYY HH:mm')
  }
});

Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).format('L')
  }
});

Vue.config.productionTip = false;
// plugin setup
Vue.use(VueRouter);
Vue.use(GlobalComponents);
Vue.use(GlobalDirectives);
Vue.use(VueToastr, {
  defaultPosition: 'toast-bottom-right',
  defaultType: 'info',
  defaultTimeout: 3000
});

//Vue.use(vueTippy, {
//  flipDuration: 0,
//  size: 'large',
//});

Vue.use(SideBar);

// configure router
const router = new VueRouter({
  routes,
  linkActiveClass: "active",
  mode: "hash",
  base: "/admin"
});

//Link Layout Definition
Object.defineProperty(Vue.prototype, "$Layouts", {
  get() {
    return [{ label: 'Üst Menü', id: 1 }, { label: 'Ana Menü', id: 2 }, { label: 'Alt Menü', id: 3 }];
  }
});

//Page Module Definition
Object.defineProperty(Vue.prototype, "$Modules", {
  get() {
    return [
      { label: 'İçerik', id: 1 },
      { label: 'Tekneler', id: 2 },
      { label: 'Hedefler', id: 3 },
      { label: 'Rotalar', id: 4 },
      { label: 'İletişim', id: 5 },
      { label: 'Blog', id: 6 },
      { label: 'Feedback', id: 7 }]
  }
});

//Fleet Categories
Object.defineProperty(Vue.prototype, "$YachtCategories", {
  get() {
    return [
      { name: 'TURKEY' },
      { name: 'GREECE' },
      { name: 'CROATIA' }]
  }
});

//DateTimePicker TR Lang
Object.defineProperty(Vue.prototype, "$lang", {
  get() {
    return {
      days: ['Paz', 'Pzt', 'Sal', 'Çar', 'Per', 'Cum', 'Cmt'],
      months: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
      pickers: ['7 gün ileri', '30 gün ileri', '7 gün geri', '30 gün geri'],
      placeholder: {
        date: 'Tarih Seçiniz'
      }
    };
  }
});


/* eslint-disable no-new */
new Vue({
  el: "#app",
  render: h => h(App),
  router,
  store,
  data() { }

});

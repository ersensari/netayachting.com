import DashboardLayout from "../components/Dashboard/Layout/DashboardLayout.vue";
// GeneralViews
import NotFound from "../components/GeneralViews/NotFoundPage.vue";

// Admin pages
import Overview from "../components/Dashboard/Views/Overview.vue";
import UserProfile from "../components/Dashboard/Views/UserProfile.vue";
import Pages from "../components/Dashboard/Views/Pages.vue";
import Links from "../components/Dashboard/Views/Links.vue";
import Settings from "../components/Dashboard/Views/Settings.vue";
import Feedbacks from "../components/Dashboard/Views/FeedbackForm.vue";
import ImageSlider from "../components/Dashboard/Views/ImageSlider.vue";
import ResumesVue from "../components/Dashboard/Views/Resumes.vue";

import Yachts from "../components/Dashboard/Views/Yachts.vue";
import Destinations from "../components/Dashboard/Views/Destinations.vue";
import Reservations from "../components/Dashboard/Views/Appointments.vue";
import ContactForm from "../components/Dashboard/Views/ContactForm.vue";

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "overview",
    children: [
      {
        path: "overview",
        name: "Yönetim Paneli",
        component: Overview
      },
      {
        path: "userprofiles",
        name: "Yöneticiler",
        component: UserProfile
      },
      {
        path: "pages",
        name: "Sayfa Tanımlama",
        component: Pages
      },
      {
        path: "links",
        name: "Link Tanımlama",
        component: Links
      },
      {
        path: "yachts",
        name: "Tekne Tanımlama",
        component: Yachts
      },
      {
        path: "destinations",
        name: "Hedef Tanımlama",
        component: Destinations
      },
      {
        path: "feedbacks",
        name: "Yorumlar",
        component: Feedbacks
      },
      {
        path: "reservations",
        name: "Rezervasyonlar",
        component: Reservations
      },
      {
        path: "contactform",
        name: "İletişim Formu",
        component: ContactForm
      },
      {
        path: "image-slider",
        name: "Anasayfa Slider",
        component: ImageSlider
      },
      {
        path: "settings",
        name: "Site Ayarları",
        component: Settings
      }
    ]
  },
  { path: "*", component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;

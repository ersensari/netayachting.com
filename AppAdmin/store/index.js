﻿import Vue from 'vue'
import Vuex from 'vuex'
import member from './modules/member'
import pages from './modules/pages'
import links from './modules/links'
import gallery from './modules/gallery'
import settings from './modules/settings'
import blogs from './modules/blog'
import bloggallery from './modules/blog-gallery'
import resumes from './modules/resumes'
import slider from './modules/slider'
import cities from './modules/cities'
import yachts from './modules/yachts'
import yachtprices from './modules/yacht-prices'
import yachtgallery from './modules/yacht-gallery'
import contactform from './modules/contact-form'
import feedbacks from './modules/feedbacks'
import currency from './modules/currency';
import appointment from './modules/appointment';


Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    member,
    pages,
    links,
    gallery,
    settings,
    blogs,
    bloggallery,
    resumes,
    slider,
    cities,
    yachts,
    yachtprices,
    yachtgallery,
    contactform,
    currency,
    feedbacks,
    appointment
  },
  strict: false
})
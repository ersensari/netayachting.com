﻿import axios from "axios";

const apiPath = "api/contactforms/";

const state = {
  all: [],
  errors: [],
  selected: null,
  isNew: false
};

const getters = {};

const mutations = {
  setItems(state, items) {
    state.all = items;
  },
  updateItem(state, item) {
    this._vm.$toastr('success', 'Kaydetme işlemi başarılı.');
  },
  addItem(state, item) {
    this._vm.$toastr('success', 'Your message has been send successfuly.');
    state.all.push(item);
    state.selected = item;
    state.isNew = false;
  },
  setErrors(state, errors) {
    state.errors = errors;
    if (errors.length > 0)
      this._vm.$toastr("error",
        errors
          .map(e => {
            return e;
          })
          .join("<br/>")
      );
  },
  setSelectedItem(state, item) {
    state.isNew = false;
    state.selected = item;
  },
  newItem(state, item) {
    state.isNew = true;
    state.selected = item;
  },
  removeItem(state, payload) {
    state.all.splice(state.all.findIndex(x => x.id == payload.id), 1);
    state.selected = null;
    this._vm.$toastr('success', 'Silme işlemi başarılı');
  }
};

///Actions
const actions = {
  getAll({ commit }) {
    commit("setErrors", []);
    axios
      .get(apiPath)
      .then(response => {
        commit("setItems", response.data);
      })
      .catch(e => {
        //commit('setErrors', e.response.data);
      });
  },

  onSave({ commit }, payload) {
    commit("setErrors", []);
    if (payload.id == 0) {
      axios({
        method: "post",
        url: apiPath,
        data: payload
      })
        .then(response => {
          commit("addItem", response.data);
        })
        .catch(e => {
          var err = [];
          if (e.response.data['Email']) {
            err = err.concat(e.response.data['Email']);
          }
          if (e.response.data['Name']) {
            err = err.concat(e.response.data['Name']);
          }
          if (e.response.data['Surname']) {
            err = err.concat(e.response.data['Surname']);
          }
          if (e.response.data['Message']) {
            err = err.concat(e.response.data['Message']);
          }
          if (e.response.data["attack-blocked"]) {
            err = err.concat(e.response.data['attack-blocked']);
          }
          commit("setErrors", err);
        });
    } else {
      axios({
        method: "put",
        url: apiPath + payload.id,
        data: payload
      })
        .then(response => {
          commit("updateItem", response.data);
        })
        .catch(e => {
          commit("setErrors", e.response.data);
        });
    }
  },
  onNew({ commit }) {
    return new Promise((resolve, reject) => {
      axios
        .get(apiPath + "-1")
        .then(response => {
          commit("newItem", response.data);
          resolve(response.data);
        })
        .catch(e => {
          commit("setErrors", e.response.data);
          reject(e);
        });
    });
  },
  onDelete({ commit }, id) {
    axios
      .delete(apiPath + id)
      .then(response => {
        commit("removeItem", response.data);
      })
      .catch(e => {
        commit("setErrors", e.response.data);
      });
  },
  onCardSelected({ commit }, payload) {
    commit("setSelectedItem", payload);
  },
  onClearSelected({ commit }) {
    commit("setSelectedItem", null);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

﻿import axios from 'axios';

const apiPath = "api/YachtPrices/";

const state = {
  all: [],
  errors: [],
  selected: null,
  isNew: false
};

const getters = {};

const mutations = {
  setAllItems(state, items) {
    state.all = items;
  },
  updateItem(state, item) {
    this._vm.$toastr('success', 'Kaydetme işlemi başarılı.');
  },
  addItem(state, item) {
    this._vm.$toastr('success', 'Kaydetme işlemi başarılı.');
    state.all.push(item);
    state.selected = item;
    state.isNew = false;
  },
  setErrors(state, errors) {
    state.errors = errors;
    if (errors.length > 0)
      this._vm.$toastr('error', errors.map(e => {
        return e.message;
      }).join("<br/>"));
  },
  setSelectedItem(state, item) {
    state.isNew = false;
    state.selected = item;
  },
  newItem(state, item) {
    state.isNew = true;
    let _today = new Date();
    item.year = _today.getFullYear();
    item.startDate = null//moment().format('L');
    item.endDate = null//moment().format('L');
    item.yachtPriceWeeks = [];
    state.selected = item;
    this._vm.$toastr('info', 'Yeni Kayıt.');
  },
  removeItem(state, payload) {
    state.all.splice(state.all.findIndex(x => x.id == payload.id), 1)
    state.selected = null;
    this._vm.$toastr('success', 'Silme işlemi başarılı');
  }
};


///Actions
const actions = {
  getAll({ commit }, criteria) {
    commit('setErrors', []);
    return new Promise((resolve, reject) => {
      axios.get(apiPath, { params: criteria })
        .then(response => {

          commit('setAllItems', response.data);
          resolve(response.data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        })
    });
  },
  onSave({ commit }, payload) {
    commit('setErrors', []);
    return new Promise((resolve, reject) => {
      payload.startDate = moment(payload.startDate).format('YYYY-MM-DD');
      payload.endDate = moment(payload.endDate).format('YYYY-MM-DD');

      payload.yachtPriceWeeks = payload.yachtPriceWeeks.map(x => {
        x.startDate = moment(x.startDate, 'DD.MM.YYYY').format("YYYY-MM-DD");
        x.endDate = moment(x.endDate, 'DD.MM.YYYY').format("YYYY-MM-DD");
        return x;
      });

      if (payload.id == 0) {
        axios({
          method: 'post',
          url: apiPath,
          data: payload
        })
          .then(response => {
            commit('addItem', response.data);
            resolve(response.data);
          })
          .catch(e => {
            commit('setErrors', e.response.data);
            reject(e);
          });
      } else {
        axios({
          method: 'put',
          url: apiPath + payload.id,
          data: payload
        })
          .then(response => {
            commit('updateItem', response.data);
            resolve(response.data);
          })
          .catch(e => {
            commit('setErrors', e.response.data);
            reject(e);
          });
      }
    });
  },
  onNew({ commit }) {
    axios.get(apiPath + '-1')
      .then(response => {
        commit('newItem', response.data);
      })
      .catch(e => {
        commit('setErrors', e.response.data);
      })

  },
  onDelete({ commit }, id) {
    axios.delete(apiPath + id)
      .then(response => {
        commit('removeItem', response.data);
      })
      .catch(e => {
        commit('setErrors', e.response.data);
      })
  },
  onItemSelected({ commit }, payload) {
    commit('setSelectedItem', payload);
  }
};


export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

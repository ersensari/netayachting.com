﻿import axios from 'axios';

const apiPath = "api/cities/";

const state = {
  allCities: {}
};

const mutations = {
  setModel(state, _model) {
    
      state.allCities = _model;
    
  }
}

const actions = {
  getAll({ commit }) {
    axios.get(apiPath)
      .then(response => {
        commit("setModel", response.data);
      });
  },
  save({ commit }, payload) {
    if (payload.id && payload.id != "") {
      let data = new FormData();

      for (var key in payload) {
        data.append(key, payload[key]);
      }
      axios({
        method: 'put',
        url: apiPath + payload.id,
        data: data
      })
        .then(response => {
          this._vm.$toastr('success', 'Kaydetme işlemi başarılı.');
        })
    }
  }
}


export default {
  namespaced: true,
  state,
  actions,
  mutations
}
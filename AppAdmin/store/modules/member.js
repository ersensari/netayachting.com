﻿import axios from 'axios';

const state = {
  all: [],
  errors: [],
  selected: null,
  isNew: false,
  authenticatedUser: null,
  isLoggedOut: false
};

const getters = {
  authenticatedUserName: state => {

    return state.authenticatedUser != null ? state.authenticatedUser.person.name + ' ' + state.authenticatedUser.person.surname : "";
  },
  isLoggedOut: state => {
    return state.isLoggedOut;
  }
};

const mutations = {
  setMembers(state, members) {
    state.all = members;
  },
  updateMember(state, member) {
    this._vm.$toastr('success', 'Kaydetme işlemi başarılı.');
  },
  addMember(state, member) {
    this._vm.$toastr('success', 'Kaydetme işlemi başarılı.');
    state.all.push(member);
    state.selected = member;
    state.isNew = false;
  },
  setErrors(state, errors) {
    state.errors = errors;
    if (errors.length > 0)
      this._vm.$toastr('error',  errors.map(e => {
        return e.message;
      }).join("<br/>"));
  },
  setSelectedMember(state, member) {
    state.isNew = false;
    state.selected = member;
  },
  newMember(state, member) {
    state.isNew = true;
    state.selected = member;
    this._vm.$toastr('info', 'Yeni kullanıcı.');
  },
  setAuthenticatedUser(state, member) {
    state.authenticatedUser = member;
  },
  setLogout(state, logout) {
    state.isLoggedOut = logout;
    this._vm.$toastr('info', 'Çıkış yapıldı.');
  }
};

const actions = {
  getAllMembers({ commit }) {
    commit('setErrors', []);

    axios.get('api/user')
      .then(response => {
        commit('setMembers', response.data);
      })
      .catch(e => {
        commit('setErrors', e.response.data);
      })
  },
  saveMember({ commit }, payload) {
    commit('setErrors', []);

    if (payload.person.id == 0) {
      axios({
        method: 'post',
        url: 'api/user',
        data: payload
      })
        .then(response => {
          commit('addMember', response.data);
        })
        .catch(e => {
          commit('setErrors', e.response.data);
        });
    } else {
      axios({
        method: 'put',
        url: 'api/user/' + payload.person.id,
        data: payload
      })
        .then(response => {
          commit('updateMember', response.data);
        })
        .catch(e => {
          commit('setErrors', e.response.data);
        });
    }

  },
  setSelectedMember({ commit }, payload) {
    commit('setSelectedMember', payload);
  },
  newMember({ commit }) {
    axios.get('api/user/-1')
      .then(response => {
        commit('newMember', response.data);
      })
      .catch(e => {
        commit('setErrors', e.response.data);
      })

  },
  getAuthenticatedUser({ commit }) {
    axios.get('api/user/-2')
      .then(response => {
        commit('setAuthenticatedUser', response.data);
      })
      .catch(e => {
        commit('setErrors', e.response.data);
      })
  },
  logout({ commit }) {
    axios.get('api/user/logout')
      .then(response => {
        commit('setLogout', true);
      })
      .catch(e => {
        commit('setErrors', e.response.data);
      })
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
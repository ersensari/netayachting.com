﻿import axios from "axios";

const apiPath = "api/bloggalleries/";

const state = {
  all: [],
  errors: [],
  selected: null,
  isNew: false
};

const getters = {};

const mutations = {
  setItems(state, items) {
    state.all = items;
  },
  updateItem(state, item) {
    this._vm.$toastr('success', 'Kaydetme işlemi başarılı.');
    state.all[state.all.findIndex(x => x.id == item.id)] = item;
  },
  addItem(state, item) {
    this._vm.$toastr('success', 'Kaydetme işlemi başarılı.');
    state.all.push(item);
    state.selected = item;
    state.isNew = false;
  },
  setErrors(state, errors) {
    state.errors = errors;
    if (errors.length > 0)
      this._vm.$toastr("error",
        errors
          .map(e => {
            return e.message;
          })
          .join("<br/>")
      );
  },
  setSelectedItem(state, item) {
    state.isNew = false;
    state.selected = item;
  },
  newItem(state, item) {
    state.isNew = true;
    state.selected = item;
    this._vm.$toastr('info', 'Yeni Kayıt.');
  },
  removeItem(state, payload) {
    state.all.splice(state.all.findIndex(x => x.id == payload.id), 1);
    state.selected = null;
   this._vm.$toastr('success', 'Silme işlemi başarılı');
  }
};

///Actions
const actions = {
  getAll({ commit }, blogId) {
    commit("setErrors", []);

    axios
      .get(apiPath, { params: { blogId: blogId } })
      .then(response => {
        commit("setItems", response.data);
      })
      .catch(e => {
        //commit('setErrors', e.response.data);
      });
  },
  onSave({ commit }, payload) {
    commit("setErrors", []);
    let data = new FormData();

    for (var key in payload) {
      data.append(key, payload[key]);
    }

    if (payload.id == 0) {
      axios({
        method: "post",
        url: apiPath,
        data: data
      })
        .then(response => {
          commit("addItem", response.data);
          commit("setSelectedItem", null);
        })
        .catch(e => {
          commit("setErrors", e.response.data);
        });
    } else {
      axios({
        method: "put",
        url: apiPath + payload.id,
        data: data
      })
        .then(response => {
          commit("updateItem", response.data);
          commit("setSelectedItem", null);
        })
        .catch(e => {
          commit("setErrors", e.response.data);
        });
    }
  },
  onNewImage({ commit }) {
    axios
      .get(apiPath + "-1")
      .then(response => {
        commit("newItem", response.data);
      })
      .catch(e => {
        commit("setErrors", e.response.data);
      });
  },
  onDelete({ commit }, id) {
    axios
      .delete(apiPath + id)
      .then(response => {
        commit("removeItem", response.data);
      })
      .catch(e => {
        commit("setErrors", e.response.data);
      });
  },
  onCardSelected({ commit }, payload) {
    commit("setSelectedItem", payload);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

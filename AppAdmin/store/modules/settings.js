﻿import axios from 'axios';

const apiPath = "api/settings/";

const state = {
  model: {}
};

const mutations = {
  setModel(state, _model) {
    if (_model.length > 0) {
      state.model = _model[0];
    }
  }
}

const actions = {
  get({ commit }) {
    return new Promise((resolve,reject)=>{
      axios.get(apiPath)
      .then(response => {
        commit("setModel", response.data);
        resolve(response.data);
      });
    });
  },
  save({ commit }, payload) {
    if (payload.id && payload.id != "") {
      let data = new FormData();

      for (var key in payload) {
        data.append(key, payload[key]);
      }
      axios({
        method: 'put',
        url: apiPath + payload.id,
        data: data
      })
        .then(response => {
          this._vm.$toastr('success', 'Kaydetme işlemi başarılı.');
        })
    }
  }
}


export default {
  namespaced: true,
  state,
  actions,
  mutations
}
﻿import axios from 'axios';
import { Promise } from 'es6-promise';

const apiPath = "api/currencies/";

const state = {
  currency: []
};

const getters = {};

const mutations = {
  setItems(state, items) {
    state.currency = items;
  }
};

///Actions
const actions = {
  get({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get(apiPath)
        .then(response => {
          commit('setItems', response.data);
          resolve(response);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        })
    })
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
import Sidebar from "./SideBar.vue";

const SidebarStore = {
  showSidebar: false,
  sidebarLinks: [{
    name: "Anasayfa",
    icon: "ti-panel",
    path: "/overview"
  }],
  sidebarLinksAll: [
    {
      name: "Anasayfa",
      icon: "ti-panel",
      path: "/overview"
    },
    {
      name: "Yöneticiler",
      icon: "ti-user",
      path: "/userprofiles"
    },
    {
      name: "Sayfa Tanımlama",
      icon: "ti-view-list-alt",
      path: "/pages"
    },
    {
      name: "Link Tanımlama",
      icon: "ti-link",
      path: "/links"
    },
    {
      name: "Tekne Tanımlama",
      icon: "fa fa-user-md",
      path: "/yachts"
    },
    {
      name: "Yorumlar",
      icon: "fa fa-rss",
      path: "/feedbacks"
    },
    {
      name: "Rezervasyonlar",
      icon: "fa fa-address-card-o",
      path: "/reservations"
    },
    {
      name: "Anasayfa Slider",
      icon: "fa fa-picture-o",
      path: "/image-slider"
    },
    {
      name: "İletişim Formu",
      icon: "fa fa-envelope-o",
      path: "/contactForm"
    },
    {
      name: "Site Yönetimi",
      icon: "fa fa-cogs",
      path: "/settings"
    }
  ],
  sidebarLinksEditor: [
    {
      name: "Anasayfa",
      icon: "ti-panel",
      path: "/overview"
    },
    {
      name: "Sayfa Tanımlama",
      icon: "ti-view-list-alt",
      path: "/pages"
    },
    {
      name: "Link Tanımlama",
      icon: "ti-link",
      path: "/links"
    },
    {
      name: "Tekne Tanımlama",
      icon: "fa fa-user-md",
      path: "/fleets"
    },
    {
      name: "Blog",
      icon: "fa fa-rss",
      path: "/blogs"
    },
    {
      name: "Anasayfa Slider",
      icon: "fa fa-picture-o",
      path: "/image-slider"
    },
    {
      name: "Site Yönetimi",
      icon: "fa fa-cogs",
      path: "/settings"
    }
  ],
  sidebarLinksCallCenter: [
    {
      name: "Anasayfa",
      icon: "ti-panel",
      path: "/overview"
    },
    {
      name: "Rezervasyonlar",
      icon: "fa fa-address-card-o",
      path: "/reservations"
    },
    {
      name: "İletişim Formu",
      icon: "fa fa-envelope-o",
      path: "/contactForm"
    }
  ],
  displaySidebar(value) {
    this.showSidebar = value;
  }
};

const SidebarPlugin = {
  install(Vue) {
    Vue.mixin({
      data() {
        return {
          sidebarStore: SidebarStore
        };
      }
    });

    Object.defineProperty(Vue.prototype, "$sidebar", {
      get() {
        return this.$root.sidebarStore;
      }
    });
    Vue.component("side-bar", Sidebar);
  }
};

export default SidebarPlugin;

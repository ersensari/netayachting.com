import Notifications from './Notifications.vue'

const NotificationStore = {
  state: [], // here the notifications will be added

  removeNotification(index) {
    this.state.splice(index, 1)
  },
  notify(notification) {
    this.state.push(notification)
  },
  notifyVue(_type, _message) {
    const icons = { "info": "ti-info-alt", "success": "ti-face-smile", "warning": "ti-alert", "danger": "ti-face-sad" };
    this.state.push({
      message: "<p>" + _message + "</p>",
      icon: icons[_type],
      horizontalAlign: 'right',
      verticalAlign: 'bottom',
      type: _type
    });
  }
}

var NotificationsPlugin = {

  install(Vue) {
    Object.defineProperty(Vue.prototype, '$notifications', {
      get() {
        return NotificationStore
      }
    })
    Vue.component('Notifications', Notifications)
  }
}

export default NotificationsPlugin

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Helpers;
using netayachting.com.Models;

namespace netayachting.com.Controllers.Api
{
  [Route("api/[controller]")]
  [Authorize(Roles = "Admin,Call_Center")]
  [ApiController]
  public class FeedbacksController : ControllerBase
  {
    private readonly dbContext _context;
    private readonly IHostingEnvironment _hostingEnvironment;


    public FeedbacksController(dbContext context, IHostingEnvironment hostingEnvironment)
    {
      _hostingEnvironment = hostingEnvironment;
      _context = context;
    }

    // GET: api/Feedbacks
    [HttpGet]
    [AllowAnonymous]
    public IEnumerable<Feedback> GetFeedbacks(bool? approved)
    {
      return _context.Feedbacks.Where(x => !approved.HasValue || x.Approved == approved);
    }

    // GET: api/Feedbacks/5
    [HttpGet("{id}")]
    [AllowAnonymous]
    public async Task<IActionResult> GetFeedback([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id == -1)
      {
        return Ok(new Feedback());
      }

      var Feedback = await _context.Feedbacks.FindAsync(id);

      if (Feedback == null)
      {
        return NotFound();
      }

      return Ok(Feedback);
    }

    // PUT: api/Feedbacks/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutFeedback([FromRoute] int id, [FromBody] Feedback Feedback)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != Feedback.Id)
      {
        return BadRequest();
      }

      _context.Entry(Feedback).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!FeedbackExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/Feedbacks
    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> PostFeedback([FromForm] Feedback Feedback)
    {

      var ip = HttpContext.Connection.RemoteIpAddress.ToString();
      var chkdate = DateTime.Now.AddMinutes(-30);
      if (_context.Feedbacks.Any(x => x.IpAddress == ip && x.CreatedDate > chkdate))
      {
        ModelState.AddModelError("attack-blocked", "Your request has been blocked");
      }

      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      Feedback.IpAddress = ip;
      Feedback.CreatedDate = DateTime.Now;

      string webRootPath = _hostingEnvironment.WebRootPath;
      #region Photo File Upload
      if (Feedback.PhotoFile != null)
      {
        string tbFileName = UploadHelper.UploadSingle(Feedback.PhotoFile, 92, "_feedback", webRootPath, "feedbacks");
        Feedback.PhotoUrl = tbFileName;
      }
      #endregion

      _context.Feedbacks.Add(Feedback);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetFeedback", new { id = Feedback.Id }, Feedback);
    }

    // DELETE: api/Feedbacks/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteFeedback([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var Feedback = await _context.Feedbacks.FindAsync(id);
      if (Feedback == null)
      {
        return NotFound();
      }

      _context.Feedbacks.Remove(Feedback);
      await _context.SaveChangesAsync();

      return Ok(Feedback);
    }

    private bool FeedbackExists(int id)
    {
      return _context.Feedbacks.Any(e => e.Id == id);
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Models;

namespace netayachting.com.Controllers.Api
{

  [Authorize(Roles = "Admin,Editor")]
  [ApiController]
  public class YachtsController : ControllerBase
  {
    private readonly dbContext _context;

    public YachtsController(dbContext context)
    {
      _context = context;
    }

    // GET: api/Yachts
    [HttpGet]
    [AllowAnonymous]
    [Route("api/[controller]")]
    public IEnumerable<Yacht> GetYachts(string destination)
    {
      return _context.Yachts.Include(x => x.Galleries).Include(x => x.YachtPrices).ThenInclude(x=>x.YachtPriceWeeks).Where(x => string.IsNullOrWhiteSpace(destination) || x.Destination.Contains(destination))
        .OrderBy(x => x.DisplayOrder);
    }

    [HttpGet]
    [AllowAnonymous]
    [Route("api/[controller]/Search")]
    public IEnumerable<Yacht> Search(string Destination, int Month, string Date, int NumOfGuests, decimal Price, string Order)
    {
      var linq = _context.Yachts.Include(x => x.Galleries).Include(x => x.YachtPrices).ThenInclude(x=>x.YachtPriceWeeks)
        .Where(x => string.IsNullOrWhiteSpace(Destination) || Destination == "all" || x.Destination.Contains(Destination))
        .Where(x => Month == 0 || x.YachtPrices.Any(y => y.YachtPriceWeeks.Any(w=> w.StartDate.Month == Month || w.EndDate.Month == Month)))
        .Where(x => string.IsNullOrWhiteSpace(Date) || Date == "all" || x.YachtPrices.Any(y => y.YachtPriceWeeks.Any(w=>w.StartDate >= DateTime.Parse(Date) || w.EndDate <= DateTime.Parse(Date))))
        .Where(x => Price == 0 || x.YachtPrices.Any(y => y.Price <= Price))
        .Where(x => NumOfGuests == 0 || x.MaxNumberOfGuests >= NumOfGuests)
        .OrderBy(x => x.DisplayOrder);


      return linq;
    }
    // GET: api/Yachts
    [HttpGet]
    [AllowAnonymous]
    [Route("api/[controller]/GetRecommendedYachts")]
    public IEnumerable<Yacht> GetRecommendedYachts()
    {
      return _context.Yachts.Include(x => x.Galleries).Where(x => x.Recommended).OrderBy(x => x.DisplayOrder);
      ;
    }

    // GET: api/Yachts/5
    [HttpGet]
    [Route("api/[controller]/{id}")]
    public async Task<IActionResult> GetYacht([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id == -1)
      {
        return Ok(new Yacht());
      }

      var yacht = await _context.Yachts.FindAsync(id);

      if (yacht == null)
      {
        return NotFound();
      }

      return Ok(yacht);
    }

    // PUT: api/Yachts/5
    [HttpPut]
    [Route("api/[controller]/{id}")]
    public async Task<IActionResult> PutYacht([FromRoute] int id, [FromBody] Yacht yacht)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != yacht.Id)
      {
        return BadRequest();
      }

      _context.Entry(yacht).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!YachtExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/Yachts
    [HttpPost]
    [Route("api/[controller]")]

    public async Task<IActionResult> PostYacht([FromBody] Yacht yacht)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      _context.Yachts.Add(yacht);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetYacht", new { id = yacht.Id }, yacht);
    }

    // DELETE: api/Yachts/5
    [HttpDelete]
    [Route("api/[controller]/{id}")]

    public async Task<IActionResult> DeleteYacht([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var yacht = await _context.Yachts.FindAsync(id);
      if (yacht == null)
      {
        return NotFound();
      }

      _context.Yachts.Remove(yacht);
      await _context.SaveChangesAsync();

      return Ok(yacht);
    }

    private bool YachtExists(int id)
    {
      return _context.Yachts.Any(e => e.Id == id);
    }
  }
}
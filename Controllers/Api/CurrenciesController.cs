﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Models;
using Newtonsoft.Json.Linq;

namespace netayachting.com.Controllers.Api
{
  [Route("api/[controller]")]
  [ApiController]
  [AllowAnonymous]
  public class CurrenciesController : ControllerBase
  {
    private readonly dbContext _context;
    const string API_KEY = "e16ffee6dc8766c910527b2356d47dd6";
    public CurrenciesController(dbContext context)
    {
      _context = context;
    }

    [HttpGet]
    public async Task<IActionResult> GetCurrencies()
    {
      //var currency = await _context.Currencies.Include(x => x.CurrencyRates).FirstOrDefaultAsync(x => x.m_date == DateTime.Today && x.m_timestamp > DateTime.Now.AddHours(-1).TimeOfDay.Ticks);
      var currency = await _context.Currencies.Include(x => x.CurrencyRates).OrderByDescending(x => x.Id).FirstOrDefaultAsync();

      bool getNew = false;
      if (currency != null)
      {
        if (DateTime.UtcNow.TimeOfDay.Subtract(currency.m_date.AddSeconds(currency.m_timestamp).TimeOfDay) < TimeSpan.FromHours(1))
        {
          return Ok(currency);
        }
        else
        {
          getNew = true;
        }
      }

      if (getNew)
      {
        WebClient wc = new WebClient();
        wc.BaseAddress = "http://data.fixer.io/api/latest";
        string jsonCurr = wc.DownloadString("?access_key=" + API_KEY + "&base=EUR&symbols=GBP,USD,TRY");
        var result = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonCurr);
        if (result["success"].Value<bool>())
        {
          Currency cr = new Currency();
          cr.m_success = result["success"].Value<bool>();
          cr.m_base = result["base"].Value<string>();
          cr.m_date = result["date"].Value<DateTime>();
          cr.m_timestamp = result["timestamp"].Value<Int64>();
          cr.CurrencyRates = new List<CurrencyRate>();
          cr.CurrencyRates.Add(new CurrencyRate() { name = "USD", amount = result["rates"]["USD"].Value<decimal>() });
          cr.CurrencyRates.Add(new CurrencyRate() { name = "GBP", amount = result["rates"]["GBP"].Value<decimal>() });
          cr.CurrencyRates.Add(new CurrencyRate() { name = "TRY", amount = result["rates"]["TRY"].Value<decimal>() });

          _context.Currencies.Add(cr);
          _context.SaveChanges();

          return Ok(cr);
        }
        else
        {
          return BadRequest();
        }
      }
      else
      {
        return BadRequest();
      }
    }

  }
}
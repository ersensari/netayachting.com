﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Models;

namespace netayachting.com.Controllers.Api
{
  [Produces("application/json")]

  [Authorize(Roles = "Admin,Editor")]

  public class BlogsController : Controller
  {
    private readonly dbContext _context;

    public BlogsController(dbContext context)
    {
      _context = context;
    }

    // GET: api/Blogs
    [HttpGet]
    [AllowAnonymous]
    [Route("api/Blogs")]
    public IEnumerable<Blog> GetBlogs(int? displayType)
    {
      return _context.Blogs.Where(x => !displayType.HasValue || x.DisplayType == displayType.Value).Include(x => x.Person).Include(x => x.Galleries).OrderByDescending(x => x.CreateDate);
    }

    // GET: api/Blogs/5
    [HttpGet]
    [AllowAnonymous]
    [Route("api/Blogs/{id}")]
    public async Task<IActionResult> GetBlog([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id == -1)
      {
        return Ok(new Blog());
      }

      var blog = await _context.Blogs.Include(x => x.Person).Include(x => x.Galleries).SingleOrDefaultAsync(m => m.Id == id);

      if (blog == null)
      {
        return NotFound();
      }

      return Ok(blog);
    }

    // PUT: api/Blogs/5
    [HttpPut]
    [Route("api/Blogs/{id}")]
    public async Task<IActionResult> PutBlog([FromRoute] int id, [FromBody] Blog blog)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != blog.Id)
      {
        return BadRequest();
      }
      if (blog.Person != null)
      {
        blog.PersonId = blog.Person.Id;
      }
      _context.Entry(blog).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!BlogExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/Blogs
    [HttpPost]
    [Route("api/Blogs")]
    public async Task<IActionResult> PostBlog([FromBody] Blog blog)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      blog.Person = null;
      _context.Blogs.Add(blog);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetBlog", new { id = blog.Id }, blog);
    }

    // DELETE: api/Blogs/5
    [HttpDelete]
    [Route("api/Blogs/{id}")]
    public async Task<IActionResult> DeleteBlog([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var blog = await _context.Blogs.SingleOrDefaultAsync(m => m.Id == id);
      if (blog == null)
      {
        return NotFound();
      }

      _context.Blogs.Remove(blog);
      await _context.SaveChangesAsync();

      return Ok(blog);
    }

    private bool BlogExists(int id)
    {
      return _context.Blogs.Any(e => e.Id == id);
    }
  }
}
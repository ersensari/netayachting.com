﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Models;

namespace netayachting.com.Controllers.Api
{
  [Route("api/[controller]")]
  [ApiController]
  [Authorize(Roles = "Admin,Call_Center")]
  public class AppointmentsController : ControllerBase
  {
    private readonly dbContext _context;

    public AppointmentsController(dbContext context)
    {
      _context = context;
    }

    // GET: api/Appointments
    [HttpGet]
    public IEnumerable<Appointment> GetAppointments()
    {
      return _context.Appointments;
    }

    // GET: api/Appointments/5
    [HttpGet("{id}")]
    [AllowAnonymous]
    public async Task<IActionResult> GetAppointment([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id == -1)
      {

        return Ok(new Appointment());
      }

      var appointment = await _context.Appointments.FindAsync(id);

      if (appointment == null)
      {
        return NotFound();
      }

      return Ok(appointment);
    }

    // PUT: api/Appointments/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutAppointment([FromRoute] int id, [FromBody] Appointment appointment)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != appointment.Id)
      {
        return BadRequest();
      }

      _context.Entry(appointment).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!AppointmentExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/Appointments
    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> PostAppointment([FromBody] Appointment appointment)
    {
      var ip = HttpContext.Connection.RemoteIpAddress.ToString();
      var chkdate = DateTime.Now.AddMinutes(-30);
      if (_context.Appointments.Any(x => x.IpAddress == ip && x.CreatedDate > chkdate))
      {
        ModelState.AddModelError("attack-blocked", "Your request has been blocked");
      }

      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      appointment.IpAddress = ip;
      appointment.CreatedDate = DateTime.Now;
      appointment.Answered = false;

      _context.Appointments.Add(appointment);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetAppointment", new { id = appointment.Id }, appointment);
    }

    // DELETE: api/Appointments/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteAppointment([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var appointment = await _context.Appointments.FindAsync(id);
      if (appointment == null)
      {
        return NotFound();
      }

      _context.Appointments.Remove(appointment);
      await _context.SaveChangesAsync();

      return Ok(appointment);
    }

    private bool AppointmentExists(int id)
    {
      return _context.Appointments.Any(e => e.Id == id);
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Helpers;
using netayachting.com.Models;

namespace netayachting.com.Controllers.Api
{
  [Produces("application/json")]
  [Authorize(Roles = "Admin,Editor")]


  public class GalleriesController : Controller
  {
    private readonly dbContext _context;
    private readonly IHostingEnvironment _hostingEnvironment;
    public GalleriesController(dbContext context, IHostingEnvironment hostingEnvironment)
    {
      _context = context;
      _hostingEnvironment = hostingEnvironment;
    }

    // GET: api/Galleries
    [HttpGet]
    [AllowAnonymous]
    [Route("api/Galleries")]
    public IEnumerable<Gallery> GetGalleries(int pageId) => _context.Galleries.Where(x => x.PageId == pageId);

    // GET: api/Galleries/5
    [HttpGet]
    [Route("api/Galleries/{id}")]
    public async Task<IActionResult> GetGallery([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      if (id == -1) //isnew
      {
        return Ok(new Gallery());
      }
      var gallery = await _context.Galleries.SingleOrDefaultAsync(m => m.Id == id);

      if (gallery == null)
      {
        return NotFound();
      }

      return Ok(gallery);
    }

    // PUT: api/Galleries/5
    [HttpPut]
    [Route("api/Galleries/{id}")]
    public async Task<IActionResult> PutGallery([FromRoute] int id, [FromForm] Gallery gallery)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != gallery.Id)
      {
        return BadRequest();
      }

      #region File Upload
      if (gallery.File != null)
      {
        string webRootPath = _hostingEnvironment.WebRootPath;
        if (!string.IsNullOrWhiteSpace(gallery.ImageLargeUrl))
        {
          System.IO.File.Delete(System.IO.Path.GetFullPath(Path.Combine(webRootPath, gallery.ImageLargeUrl)));
        }
        if (!string.IsNullOrWhiteSpace(gallery.ImageThumbUrl))
        {
          System.IO.File.Delete(System.IO.Path.GetFullPath(Path.Combine(webRootPath, gallery.ImageThumbUrl)));
        }
        string tbFileName = UploadHelper.UploadSingle(gallery.File, 85, "_thumb", webRootPath, "images", "gallary");
        string lgFileName = UploadHelper.UploadSingle(gallery.File, 1920, "_large", webRootPath, "images", "gallary");

        gallery.ImageThumbUrl = tbFileName;
        gallery.ImageLargeUrl = lgFileName;
      }
      #endregion

      _context.Entry(gallery).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!GalleryExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return Ok(gallery);
    }

    // POST: api/Galleries
    [HttpPost]
    [Route("api/Galleries")]
    public async Task<IActionResult> PostGallery([FromForm] Gallery gallery)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      try
      {
        #region File Upload

        if (gallery.File != null)
        {
          string webRootPath = _hostingEnvironment.WebRootPath;
          string tbFileName = UploadHelper.UploadSingle(gallery.File, 85, "_thumb", webRootPath, "images", "gallary");
          string lgFileName = UploadHelper.UploadSingle(gallery.File, 1920, "_large", webRootPath, "images", "gallary");

          gallery.ImageThumbUrl = tbFileName;
          gallery.ImageLargeUrl = lgFileName;
        }
        #endregion

        _context.Galleries.Add(gallery);
        await _context.SaveChangesAsync();

      }
      catch (FileUploadException ex)
      {
        ModelState.AddModelError("gallery-post", ex.Message);
        return BadRequest(ModelState);
      }
      return CreatedAtAction("GetGallery", new { id = gallery.Id }, gallery);
    }

    // DELETE: api/Galleries/5
    [HttpDelete]
    [Route("api/Galleries/{id}")]
    public async Task<IActionResult> DeleteGallery([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var gallery = await _context.Galleries.SingleOrDefaultAsync(m => m.Id == id);
      if (gallery == null)
      {
        return NotFound();
      }

      try
      {
        if (!string.IsNullOrWhiteSpace(gallery.ImageLargeUrl))
        {
          System.IO.File.Delete(System.IO.Path.Combine(_hostingEnvironment.WebRootPath, gallery.ImageLargeUrl));
        }
        if (!string.IsNullOrWhiteSpace(gallery.ImageThumbUrl))
        {
          System.IO.File.Delete(System.IO.Path.Combine(_hostingEnvironment.WebRootPath, gallery.ImageThumbUrl));
        }
      }
      catch (Exception)
      {
      }
      _context.Galleries.Remove(gallery);
      await _context.SaveChangesAsync();

      return Ok(gallery);
    }

    private bool GalleryExists(int id)
    {
      return _context.Galleries.Any(e => e.Id == id);
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Models;

namespace netayachting.com.Controllers.Api
{
  [Route("api/[controller]")]
  [Authorize(Roles = "Admin,Call_Center")]
  [ApiController]
  public class ContactFormsController : ControllerBase
  {
    private readonly dbContext _context;

    public ContactFormsController(dbContext context)
    {
      _context = context;
    }

    // GET: api/ContactForms
    [HttpGet]
    public IEnumerable<ContactForm> GetContactForms()
    {
      return _context.ContactForms;
    }

    // GET: api/ContactForms/5
    [HttpGet("{id}")]
    [AllowAnonymous]
    public async Task<IActionResult> GetContactForm([FromRoute] int id)
    {
      if (!ModelState.IsValid) 
      {
        return BadRequest(ModelState);
      }

      if (id == -1)
      {
        return Ok(new ContactForm());
      }

      var contactForm = await _context.ContactForms.FindAsync(id);

      if (contactForm == null)
      {
        return NotFound();
      }

      return Ok(contactForm);
    }

    // PUT: api/ContactForms/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutContactForm([FromRoute] int id, [FromBody] ContactForm contactForm)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != contactForm.Id)
      {
        return BadRequest();
      }

      _context.Entry(contactForm).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!ContactFormExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/ContactForms
    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> PostContactForm([FromBody] ContactForm contactForm)
    {

      var ip = HttpContext.Connection.RemoteIpAddress.ToString();
      var chkdate = DateTime.Now.AddMinutes(-30);
      if (_context.ContactForms.Any(x => x.IpAddress == ip && x.CreatedDate > chkdate))
      {
        ModelState.AddModelError("attack-blocked", "Your request has been blocked");
      }

      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      contactForm.IpAddress = ip;
      contactForm.CreatedDate = DateTime.Now;

      _context.ContactForms.Add(contactForm);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetContactForm", new { id = contactForm.Id }, contactForm);
    }

    // DELETE: api/ContactForms/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteContactForm([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var contactForm = await _context.ContactForms.FindAsync(id);
      if (contactForm == null)
      {
        return NotFound();
      }

      _context.ContactForms.Remove(contactForm);
      await _context.SaveChangesAsync();

      return Ok(contactForm);
    }

    private bool ContactFormExists(int id)
    {
      return _context.ContactForms.Any(e => e.Id == id);
    }
  }
}
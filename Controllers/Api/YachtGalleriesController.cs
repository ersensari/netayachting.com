﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Helpers;
using netayachting.com.Models;

namespace netayachting.com.Controllers.Api {
  [Produces ("application/json")]
  [Authorize (Roles = "Admin,Editor")]

  public class YachtGalleriesController : Controller {
    private readonly dbContext _context;
    private readonly IHostingEnvironment _hostingEnvironment;
    public YachtGalleriesController (dbContext context, IHostingEnvironment hostingEnvironment) {
      _context = context;
      _hostingEnvironment = hostingEnvironment;
    }

    // GET: api/YachtGalleries
    [HttpGet]
    [AllowAnonymous]
    [Route ("api/YachtGalleries")]
    public IEnumerable<YachtGallery> GetYachtGalleries (int yachtId) => _context.YachtGalleries.Where (x => x.YachtId == yachtId);

    // GET: api/YachtGalleries/5
    [HttpGet]
    [Route ("api/YachtGalleries/{id}")]
    public async Task<IActionResult> GetYachtGallery ([FromRoute] int id) {
      if (!ModelState.IsValid) {
        return BadRequest (ModelState);
      }
      if (id == -1) //isnew
      {
        return Ok (new YachtGallery ());
      }
      var gallery = await _context.YachtGalleries.SingleOrDefaultAsync (m => m.Id == id);

      if (gallery == null) {
        return NotFound ();
      }

      return Ok (gallery);
    }

    // PUT: api/YachtGalleries/5
    [HttpPut]
    [Route ("api/YachtGalleries/{id}")]
    public async Task<IActionResult> PutYachtGallery ([FromRoute] int id, [FromForm] YachtGallery gallery) {
      if (!ModelState.IsValid) {
        return BadRequest (ModelState);
      }

      if (id != gallery.Id) {
        return BadRequest ();
      }

      #region File Upload
      if (gallery.File != null) {
        string webRootPath = _hostingEnvironment.WebRootPath;
        if (!string.IsNullOrWhiteSpace (gallery.ImageLargeUrl)) {
          System.IO.File.Delete (System.IO.Path.GetFullPath (Path.Combine (webRootPath, gallery.ImageLargeUrl)));
        }
        if (!string.IsNullOrWhiteSpace (gallery.ImageThumbUrl)) {
          System.IO.File.Delete (System.IO.Path.GetFullPath (Path.Combine (webRootPath, gallery.ImageThumbUrl)));
        }
        string tbFileName = UploadHelper.UploadSingle (gallery.File, 85, "_thumb", webRootPath, "images", "yacht-gallary");
        string lgFileName = UploadHelper.UploadSingle (gallery.File, 1920, "_large", webRootPath, "images", "yacht-gallary");

        gallery.ImageThumbUrl = tbFileName;
        gallery.ImageLargeUrl = lgFileName;
      }
      #endregion

      _context.Entry (gallery).State = EntityState.Modified;

      try {
        await _context.SaveChangesAsync ();
      } catch (DbUpdateConcurrencyException) {
        if (!GalleryExists (id)) {
          return NotFound ();
        } else {
          throw;
        }
      }

      return Ok (gallery);
    }

    // POST: api/YachtGalleries
    [HttpPost]
    [Route ("api/YachtGalleries")]
    public async Task<IActionResult> PostYachtGallery ([FromForm] YachtGallery gallery) {
      if (!ModelState.IsValid) {
        return BadRequest (ModelState);
      }

      try {
        #region File Upload

        if (gallery.File != null) {
          string webRootPath = _hostingEnvironment.WebRootPath;
          string tbFileName = UploadHelper.UploadSingle (gallery.File, 85, "_thumb", webRootPath, "images", "yacht-gallary");
          string lgFileName = UploadHelper.UploadSingle (gallery.File, 1920, "_large", webRootPath, "images", "yacht-gallary");

          gallery.ImageThumbUrl = tbFileName;
          gallery.ImageLargeUrl = lgFileName;
        }
        #endregion

        _context.YachtGalleries.Add (gallery);
        await _context.SaveChangesAsync ();

      } catch (FileUploadException ex) {
        ModelState.AddModelError ("gallery-post", ex.Message);
        return BadRequest (ModelState);
      }
      return CreatedAtAction ("GetYachtGallery", new { id = gallery.Id }, gallery);
    }

    // DELETE: api/YachtGalleries/5
    [HttpDelete]
    [Route ("api/YachtGalleries/{id}")]
    public async Task<IActionResult> DeleteGallery ([FromRoute] int id) {
      if (!ModelState.IsValid) {
        return BadRequest (ModelState);
      }

      var gallery = await _context.YachtGalleries.SingleOrDefaultAsync (m => m.Id == id);
      if (gallery == null) {
        return NotFound ();
      }

      try {
        if (!string.IsNullOrWhiteSpace (gallery.ImageLargeUrl)) {
          System.IO.File.Delete (System.IO.Path.Combine (_hostingEnvironment.WebRootPath, gallery.ImageLargeUrl));
        }
        if (!string.IsNullOrWhiteSpace (gallery.ImageThumbUrl)) {
          System.IO.File.Delete (System.IO.Path.Combine (_hostingEnvironment.WebRootPath, gallery.ImageThumbUrl));
        }
      } catch (Exception) { }
      _context.YachtGalleries.Remove (gallery);
      await _context.SaveChangesAsync ();

      return Ok (gallery);
    }

    private bool GalleryExists (int id) {
      return _context.YachtGalleries.Any (e => e.Id == id);
    }
  }
}
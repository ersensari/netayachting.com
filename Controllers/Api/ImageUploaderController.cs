﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using netayachting.com.Helpers;

namespace netayachting.com.Controllers.Api
{
  [Produces("application/json")]
  public class ImageUploaderController : Controller
  {
    private readonly IHostingEnvironment _hostingEnvironment;
    public ImageUploaderController(IHostingEnvironment hostingEnvironment)
    {
      _hostingEnvironment = hostingEnvironment;
    }

    [HttpGet]
    [Route("api/ImageUploader")]
    public ActionResult get()
    {
      List<uploadModel> files = new List<uploadModel>();
      foreach (var file in Directory.GetFiles(Path.Combine(_hostingEnvironment.WebRootPath, "images", "uploads"), "*.*", SearchOption.AllDirectories))
      {
        if (Regex.IsMatch(file, @".jpg|.png|.gif|.bmp$"))
        {
          files.Add(new uploadModel(file.Replace(_hostingEnvironment.WebRootPath, "")));
        }
      }

      return Ok(files);
    }

    [HttpPost]
    [Route("api/ImageUploader")]
    public ActionResult upload()
    {
      if (Request.Form.Files.Count > 0)
      {
        var file = Request.Form.Files[0];
        string webRootPath = _hostingEnvironment.WebRootPath;
        string tbFileName = UploadHelper.UploadSingle(file, 0, "_upload", webRootPath, "images", "uploads");
        return Ok(new uploadModel(tbFileName.Replace("\\", "/")));
      }
      else
      {
        return Ok(new uploadModel(""));
      }
    }

    [HttpPost]
    [Route("api/ImageUploader/delete")]
    public ActionResult deleteFile(string link)
    {
      string webRootPath = _hostingEnvironment.WebRootPath;
      System.IO.File.Delete(webRootPath + link);
      return Ok();
    }
  }

  [Serializable]
  public class uploadModel
  {
    public uploadModel(string _link)
    {
      this.link = _link;
      this.url = _link;
    }
    public string link { get; set; }
    public string url { get; set; }
  }
}
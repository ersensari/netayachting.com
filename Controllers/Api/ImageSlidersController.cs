﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Helpers;
using netayachting.com.Models;

namespace netayachting.com.Controllers.Api
{
  [ApiController]
  [Route("api/ImageSliders")]
  [Authorize(Roles = "Admin,Editor")]

  public class ImageSlidersController : ControllerBase
  {
    private readonly dbContext _context;
    private readonly IHostingEnvironment _hostingEnvironment;

    public ImageSlidersController(dbContext context, IHostingEnvironment hostingEnvironment)
    {
      _hostingEnvironment = hostingEnvironment;
      _context = context;
    }

    // GET: api/ImageSliders
    [HttpGet]
    [AllowAnonymous]
    public IEnumerable<ImageSlider> GetImageSliders()
    {
      return _context.ImageSliders.OrderBy(x => x.DisplayOrder);
    }

    // GET: api/ImageSliders/5
    [HttpGet("{id}")]
    [AllowAnonymous]
    public async Task<IActionResult> GetImageSlider([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id == -1)
      {
        return Ok(new ImageSlider());
      }

      var imageSlider = await _context.ImageSliders.FindAsync(id);

      if (imageSlider == null)
      {
        return NotFound();
      }

      return Ok(imageSlider);
    }

    // PUT: api/ImageSliders/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutImageSlider([FromRoute] int id, [FromForm] ImageSlider imageSlider)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != imageSlider.Id)
      {
        return BadRequest();
      }

      try
      {
        #region File Upload

        if (imageSlider.ImageFile != null)
        {
          string webRootPath = _hostingEnvironment.WebRootPath;
          string lgFileName = UploadHelper.UploadSingle(imageSlider.ImageFile, 1920, "_slider", webRootPath, "images", "image-slider");

          imageSlider.ImageUrl = lgFileName;
        }
        #endregion

        _context.Entry(imageSlider).State = EntityState.Modified;

      }
      catch (FileUploadException ex)
      {
        ModelState.AddModelError("imageslider-post", ex.Message);
        return BadRequest(ModelState);
      }
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!ImageSliderExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return await this.GetImageSlider(imageSlider.Id);
    }

    // POST: api/ImageSliders
    [HttpPost]
    public async Task<IActionResult> PostImageSlider([FromForm] ImageSlider imageSlider)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }


      try
      {
        #region File Upload

        if (imageSlider.ImageFile != null)
        {
          string webRootPath = _hostingEnvironment.WebRootPath;
          string lgFileName = UploadHelper.UploadSingle(imageSlider.ImageFile, 1920, "_slider", webRootPath, "images", "image-slider");

          imageSlider.ImageUrl = lgFileName;
        }
        #endregion

        _context.ImageSliders.Add(imageSlider);

      }
      catch (FileUploadException ex)
      {
        ModelState.AddModelError("imageslider-post", ex.Message);
        return BadRequest(ModelState);
      }

      await _context.SaveChangesAsync();

      return CreatedAtAction("GetImageSlider", new { id = imageSlider.Id }, imageSlider);
    }

    // DELETE: api/ImageSliders/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteImageSlider([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var imageSlider = await _context.ImageSliders.FindAsync(id);
      if (imageSlider == null)
      {
        return NotFound();
      }

      _context.ImageSliders.Remove(imageSlider);
      await _context.SaveChangesAsync();

      return Ok(imageSlider);
    }

    private bool ImageSliderExists(int id)
    {
      return _context.ImageSliders.Any(e => e.Id == id);
    }
  }
}
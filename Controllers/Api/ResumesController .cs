﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Helpers;
using netayachting.com.Models;

namespace netayachting.com.Controllers.Api
{
  [Produces("application/json")]
  [Route("api/Resumes")]
  [Authorize(Roles = "Admin,Insan_Kaynaklari")]

  public class ResumesController : Controller
  {
    private readonly dbContext _context;
    private readonly IHostingEnvironment _hostingEnvironment;


    public ResumesController(dbContext context, IHostingEnvironment hostingEnvironment)
    {
      _hostingEnvironment = hostingEnvironment;
      _context = context;
    }

    // GET: api/Resumes
    [HttpGet]
    public IEnumerable<Resume> GetResumes()
    {
      return _context.Resumes.Include(x => x.City).ThenInclude(x => x.Towns).Include(x => x.Town);
    }

    // GET: api/Resumes/5
    [HttpGet("{id}")]
    public async Task<IActionResult> GetResume([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      if (id == -1)
      {
        return Ok(new Resume());
      }
      var Resume = await _context.Resumes.Include(x => x.City).ThenInclude(x => x.Towns).Include(x => x.Town).SingleOrDefaultAsync(m => m.Id == id);

      if (Resume == null)
      {
        return NotFound();
      }

      return Ok(Resume);
    }

    // PUT: api/Resumes/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutResume([FromRoute] int id, [FromForm] Resume Resume)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != Resume.Id)
      {
        return BadRequest();
      }
      Resume.Town = null;
      Resume.City = null;
      _context.Entry(Resume).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!ResumeExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return await GetResume(Resume.Id);
    }

    // POST: api/Resumes
    [HttpPost]
    public async Task<IActionResult> PostResume([FromForm] Resume Resume)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      Resume.Town = null;
      Resume.City = null;

      string webRootPath = _hostingEnvironment.WebRootPath;
      #region CV File Upload
      if (Resume.ResumeFile!= null)
      {
        string tbFileName = UploadHelper.UploadSingle(Resume.ResumeFile, 0, "_resume", webRootPath, "resumes");
        Resume.ResumeFileUrl = tbFileName;
      }
      #endregion

      _context.Resumes.Add(Resume);

      await _context.SaveChangesAsync();

      return await GetResume(Resume.Id);
    }

    // DELETE: api/Resumes/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteResume([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var Resume = await _context.Resumes.SingleOrDefaultAsync(m => m.Id == id);
      if (Resume == null)
      {
        return NotFound();
      }

      _context.Resumes.Remove(Resume);
      await _context.SaveChangesAsync();

      return Ok(Resume);
    }

    private bool ResumeExists(int id)
    {
      return _context.Resumes.Any(e => e.Id == id);
    }
  }
}
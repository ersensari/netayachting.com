﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Helpers;
using netayachting.com.Models;

namespace mmtamerican.com.Controllers.Api
{
  [Produces("application/json")]
  [Route("api/Settings")]
  [Authorize(Roles = "Admin,Editor")]
  public class SettingsController : Controller
  {
    private readonly dbContext _context;
    private readonly IHostingEnvironment _hostingEnvironment;


    public SettingsController(dbContext context, IHostingEnvironment hostingEnvironment)
    {
      _hostingEnvironment = hostingEnvironment;
      _context = context;
    }

    // GET: api/Settings
    [HttpGet]
    [AllowAnonymous]
    public IEnumerable<Setting> GetSettings()
    {
      return _context.Settings;
    }

    // GET: api/Settings/5
    [HttpGet("{id}")]
    [AllowAnonymous]
    public async Task<IActionResult> GetSetting([FromRoute] string id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      Guid _id = new Guid(id);
      var setting = await _context.Settings.SingleOrDefaultAsync(m => m.Id == _id);

      if (setting == null)
      {
        return NotFound();
      }

      return Ok(setting);
    }

    // PUT: api/Settings/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutSetting([FromRoute] string id, [FromForm] Setting setting)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (new Guid(id) != setting.Id)
      {
        return BadRequest();
      }

      string webRootPath = _hostingEnvironment.WebRootPath;
      #region Top Bar Logo File Upload
      if (setting.TopBarLogoFile != null)
      {
        if (!string.IsNullOrWhiteSpace(setting.TopBarLogo))
        {
          System.IO.File.Delete(System.IO.Path.GetFullPath(Path.Combine(webRootPath, setting.TopBarLogo)));
        }
        string tbFileName = UploadHelper.UploadSingle(setting.TopBarLogoFile, 0, "_logo", webRootPath, "images");
        setting.TopBarLogo = tbFileName;
      }
      #endregion

      #region Footer Bar Logo File Upload
      if (setting.FooterBarLogoFile != null)
      {
        if (!string.IsNullOrWhiteSpace(setting.FooterBarLogo))
        {
          System.IO.File.Delete(System.IO.Path.GetFullPath(Path.Combine(webRootPath, setting.FooterBarLogo)));
        }
        string ftFileName = UploadHelper.UploadSingle(setting.FooterBarLogoFile, 0, "_logo", webRootPath, "images");

        setting.FooterBarLogo = ftFileName;
      }
      #endregion
      _context.Entry(setting).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!SettingExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/Settings
    [HttpPost]
    public async Task<IActionResult> PostSetting([FromBody] Setting setting)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      _context.Settings.Add(setting);
      await _context.SaveChangesAsync();

      return CreatedAtAction("GetSetting", new { id = setting.Id }, setting);
    }

    // DELETE: api/Settings/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteSetting([FromRoute] string id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      Guid _id = new Guid(id);

      var setting = await _context.Settings.SingleOrDefaultAsync(m => m.Id == _id);
      if (setting == null)
      {
        return NotFound();
      }

      _context.Settings.Remove(setting);
      await _context.SaveChangesAsync();

      return Ok(setting);
    }

    private bool SettingExists(string id)
    {
      Guid _id = new Guid(id);

      return _context.Settings.Any(e => e.Id == _id);
    }
  }
}
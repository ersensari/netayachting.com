﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using netayachting.com.Data;
using netayachting.com.Models;

namespace netayachting.com.Api {
  [Route ("api/[controller]")]
  [ApiController]
  [Authorize (Roles = "Admin,Editor")]

  public class YachtPricesController : ControllerBase {
    private readonly dbContext _context;

    public YachtPricesController (dbContext context) {
      _context = context;
    }

    // GET: api/YachtPrices
    [HttpGet]
    [AllowAnonymous]
    public IEnumerable<YachtPrice> GetYachtPrices (int year, int? yachtId) {

      var prices = _context.YachtPrices.Include (x => x.YachtPriceWeeks).Where (x => x.Year == year);
      if (yachtId.HasValue) {
        prices = prices.Where (x => x.YachtId == yachtId.Value);
      }

      return prices;
    }

    // GET: api/YachtPrices/5
    [HttpGet ("{id}")]
    public IActionResult GetYachtPrice ([FromRoute] int id) {
      if (!ModelState.IsValid) {
        return BadRequest (ModelState);
      }

      if (id == -1) {
        return Ok (new YachtPrice ());
      }

      var yachtPrice = _context.YachtPrices.Include (x => x.YachtPriceWeeks).Where (x => x.Id == id).FirstOrDefault();

      if (yachtPrice == null) {
        return NotFound ();
      }

      return Ok (yachtPrice);
    }

    // PUT: api/YachtPrices/5
    [HttpPut ("{id}")]
    public async Task<IActionResult> PutYachtPrice ([FromRoute] int id, [FromBody] YachtPrice yachtPrice) {
      if (!ModelState.IsValid) {
        return BadRequest (ModelState);
      }

      if (id != yachtPrice.Id) {
        return BadRequest ();
      }

      foreach (var item in yachtPrice.YachtPriceWeeks) {
        if (item.Id == 0) {
          _context.YachtPriceWeeks.Add (item);
        } else {
          _context.Entry (item).State = EntityState.Modified;
        }
      }

      _context.Entry (yachtPrice).State = EntityState.Modified;

      try {
        await _context.SaveChangesAsync ();
      } catch (DbUpdateConcurrencyException) {
        if (!YachtPriceExists (id)) {
          return NotFound ();
        } else {
          throw;
        }
      }

      return Ok (yachtPrice);
    }

    // POST: api/YachtPrices
    [HttpPost]
    public async Task<IActionResult> PostYachtPrice ([FromBody] YachtPrice yachtPrice) {
      if (!ModelState.IsValid) {
        return BadRequest (ModelState);
      }
      foreach (var item in yachtPrice.YachtPriceWeeks) {
        _context.YachtPriceWeeks.Add (item);
      }

      _context.YachtPrices.Add (yachtPrice);
      await _context.SaveChangesAsync ();

      return CreatedAtAction ("GetYachtPrice", new { id = yachtPrice.Id }, yachtPrice);
    }

    // DELETE: api/YachtPrices/5
    [HttpDelete ("{id}")]
    public async Task<IActionResult> DeleteYachtPrice ([FromRoute] int id) {
      if (!ModelState.IsValid) {
        return BadRequest (ModelState);
      }

      var yachtPrice = await _context.YachtPrices.FindAsync (id);
      if (yachtPrice == null) {
        return NotFound ();
      }

      var yachtPriceWeeks = _context.YachtPriceWeeks.Where (x => x.YachtPriceId == id);
      foreach (var item in yachtPriceWeeks) {
        _context.YachtPriceWeeks.Remove (item);
      }

      _context.YachtPrices.Remove (yachtPrice);
      await _context.SaveChangesAsync ();

      return Ok (yachtPrice);
    }

    private bool YachtPriceExists (int id) {
      return _context.YachtPrices.Any (e => e.Id == id);
    }
  }
}
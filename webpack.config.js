"use strict";
const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const bundleOutputDir = "./wwwroot/dist";
const hotMiddlewareScript =
  "webpack-hot-middleware/client?path=__webpack_hmr";

module.exports = env => {
  const isDevBuild = !(env && env.prod);
  return [
    {
      name: "neta",
      mode: isDevBuild ? "development" : "production",
      stats: { modules: false },
      entry: {
        main: ["./App/index.js"],
        admin: ["./AppAdmin/index.js"]
      },
      resolve: {
        extensions: [".js", ".vue"],
        alias: { vue: "vue/dist/vue.js" }
      },
      output: {
        path: path.join(__dirname, bundleOutputDir),
        filename: "[name].js",
        publicPath: "/dist/"
      },
      module: {
        rules: [
          { test: /\.vue$/, use: "vue-loader" },

          {
            test: /\.(css|sass|scss)$/,
            use: isDevBuild
              ? ["vue-style-loader", "css-loader", "sass-loader"]
              : ExtractTextPlugin.extract({ use: "css-loader" })
          },
          {
            test: /\.(png|jpg|jpeg|gif|svg|ttf|eot|woff2|woff|otf)$/,
            use: "url-loader?limit=25000"
          },
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader",
              options: {
                presets: ["es2015"]
              }
            }
          }
          //{
          //  test: /\.(png|jpg|jpeg|gif|svg|ttf|eot|woff2|woff|otf)$/,
          //  loader: "file-loader",
          //  options: {
          //    name: "[name].[ext]?[hash]"
          //  }
          //}
        ]
      },
      plugins: [
        new VueLoaderPlugin(),
        new webpack.ProvidePlugin({
          $: "jquery",
          jquery: "jquery",
          "window.jQuery": "jquery",
          jQuery: "jquery",
          moment: "moment"
        })
      ].concat(
        isDevBuild
          ? [
              // Plugins that apply in development builds only
              new webpack.SourceMapDevToolPlugin({
                filename: "[file].map", // Remove this line if you prefer inline source maps
                moduleFilenameTemplate: path.relative(
                  bundleOutputDir,
                  "[resourcePath]"
                ) // Point sourcemap entries to the original file locations on disk
              })
            ]
          : [
              // Plugins that apply in production builds only
              new webpack.optimize.UglifyJsPlugin()
              //new ExtractTextPlugin('site.css')
            ]
      )
    }
  ];
};
